<?php
require "bootstrap.php";
use Chatter\Models\Message;
use Chatter\Models\User;
use Chatter\Middleware\Logging;
use Chatter\Models\Product;

$app = new \Slim\App();
$app->add(new Logging());
$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});

$app->get('/customers/{cnumber}', function($request, $response,$args){
   return $response->write('Customer '.$args['cnumber']);
});

$app->get('/customers/{cnumber}/products/{pnumber}', function($request, $response,$args){
   return $response->write('Customer '.$args['cnumber' ]. ' Product '.$args['pnumber' ]);
});

$app->get('/messages', function($request, $response,$args){
   $_message = new Message();
   $messages = $_message->all();
   $payload = [];
   foreach($messages as $msg){
        $payload[$msg->id] = [
            'body'=>$msg->body,
            'user_id'=>$msg->user_id,
            'created_at'=>$msg->created_at
        ];
   }
   return $response->withStatus(200)->withJson($payload);
});


$app->get('/messages/{id}', function($request, $response,$args){
$_id = $args['id'];
$message = Message::find($_id);

   return $response->withStatus(200)->withJson($message);
});

$app->post('/messages', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message','');
    $userid = $request->getParsedBodyParam('userid','');
   $_message = new Message();
   $_message->body = $message;
   $_message->user_id = $userid;
   $_message->save();
   if($_message->id){
       $payload = ['message_id'=>$_message->id];
       return $response->withStatus(201)->withJson($payload);
   }
   else{
       return $response->withStatus(400);
   }
});


$app->delete('/messages/{message_id}', function($request, $response,$args){
    $_message = Message::find($args['message_id']);
    $_message->delete();
    if($_message->exists){
        return $response->withStatus(400);
    }
    else{
         return $response->withStatus(200);
    }
});

$app->put('/messages/{message_id}', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message','');
    $_message = Message::find($args['message_id']);
    //die("message id is " . $_message->id);
    $_message->body = $message;
    if($_message->save()){
        $payload = ['message_id'=>$_message->id,"result"=>"The message has been updated successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
         return $response->withStatus(400);
    }
});

//messages bulk---------------------------------------------------------
$app->post('/messages/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();
    Message::insert($payload);
    return $response->withStatus(200)->withJson($payload);

});

$app->post('/users/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();//מה שאני מקבל זה רק ג'ייסון והפקודה הופכת את פיילווד למערך 
    User::insert($payload);
    return $response->withStatus(201)->withJson($payload);
});

$app->delete('/users/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();//מה שאני מקבל זה רק ג'ייסון והפקודה הופכת את פיילווד למערך 

    User::trash($payload);
    return $response->withStatus(201)->withJson($payload);
});


//user HW---------------------------------------------------------
$app->get('/users', function($request, $response,$args){
   $token=$request->getQueryParam('token','');
   if($token=='true'){
   $_user = new User();
   $users = $_user->all();
   $payload = [];
   foreach($users as $usr){
        $payload[$usr->id] = [
            'id'=>$usr->id,
            'username'=>$usr->username,
            'email'=>$usr->email
        ];
   }
   return $response->withStatus(200)->withJson($payload);
   }
   return $response->withStatus(400);
});

$app->post('/users', function($request, $response,$args){
    $username = $request->getParsedBodyParam('name','');
    $email = $request->getParsedBodyParam('email','');
    $password = $request->getParsedBodyParam('password','');
    $token = $request->getParsedBodyParam('token','');
    if($token=="true"){
   $_user = new User();
   $_user->username = $username;
   $_user->email = $email;
   $_user->password = $password;
   $_user->save();
   if($_user->id){
       $payload = ['user id: '=>$_user->id];
       return $response->withStatus(201)->withJson($payload);
   }
   }
   else{
       return $response->withStatus(400);
   }
});


$app->delete('/user/{id}', function($request, $response,$args){
    $user = User::find($args['id']);
    $user->delete();
    if($user->exists){
        return $response->withStatus(400);
    }
    else{
         return $response->withStatus(200);
    }
});

$app->get('/user/{key}', function($request, $response,$args){
    $_id = $args['key'];
    $user = User::find($_id);
    
    return $response->withStatus(200)->withJson($user);
    
     });

     $app->put('/user/{user_id}', function($request, $response, $args){
        $email = $request->getParsedBodyParam('email','');
        $username = $request->getParsedBodyParam('username','');
        $_user = User::find($args['user_id']);
        $_user->email = $email;
        $_user->username = $username;
       
        if($_user->save()){
            $payload = ['user_id' => $_user->id,"result" => "The user has been updates successfuly"];
            return $response->withStatus(200)->withJson($payload);
        }
        else{
            return $response->withStatus(400);
        }
    });


$app->post('/auth', function($request, $response,$args){
    $name  = $request->getParsedBodyParam('user','');
    $password = $request->getParsedBodyParam('password','');    
    $_user = User::where('username', '=', $name)->where('password', '=', $password)->get();
    
    if($_user[0]->id){
       $payload = ['success'=>true];
        return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }
    else{
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }

});



//--------------------------

$app->get('/products', function($request, $response,$args){
   $_product = new Product();
   $products = $_product->all();
   $payload = [];
   foreach($products as $prd){
        $payload[$prd->id] = [
            'name'=>$prd->name,
            'price'=>$prd->price,
            //'created_at'=>$prd->created_at
        ];
   }
   return $response->withStatus(200)->withJson($payload);
});


     $app->put('/product/{product_id}', function($request, $response, $args){
        $name = $request->getParsedBodyParam('name','');
        $price = $request->getParsedBodyParam('price','');
        $_product = Product::find($args['product_id']);
        $_product->name = $name;
        $_product->price = $price;
       
        if($_product->save()){
            $payload = ['product_id' => $_product->id,"result" => "The product has been updated successfuly"];
            return $response->withStatus(200)->withJson($payload);
        }
        else{
            return $response->withStatus(400);
        }
    });


    $app->get('/product/{key}', function($request, $response,$args){
    $_id = $args['key'];
    $product = Product::find($_id);
    
    return $response->withStatus(200)->withJson($product);
    
     });



     $app->post('/products/search', function($request, $response,$args){
    $name  = $request->getParsedBodyParam('name','');
    $products = Product::where('name', 'like','%'.$name.'%')->get();
    
    return $response->withStatus(200)->withJson($products);
    
     });

//--------------------------
$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->run();

